let btn1 = document.getElementById("btnKhoi1");
let btn2 = document.getElementById("btnKhoi2");
btn1.addEventListener("click", calAvg);
btn2.addEventListener("click", calAvg);
function calAvg() {
  let toan = document.getElementById("inpToan").value * 1;
  let ly = document.getElementById("inpLy").value * 1;
  let hoa = document.getElementById("inpHoa").value * 1;
  let van = document.getElementById("inpVan").value * 1;
  let su = document.getElementById("inpSu").value * 1;
  let dia = document.getElementById("inpDia").value * 1;
  let english = document.getElementById("inpEnglish").value * 1;
  function tinhDiemTrungBinh(...scores) {
    //tạo hàm tính trung bình
    let sum = 0;
    let dtb = 0;
    scores.forEach((item) => {
      sum += item;
    });
    dtb = sum / scores.length;
    return dtb;
  }
  document.getElementById("tbKhoi1").innerHTML = tinhDiemTrungBinh(
    toan,
    ly,
    hoa
  ).toFixed(2);
  document.getElementById("tbKhoi2").innerHTML = tinhDiemTrungBinh(
    van,
    su,
    dia,
    english
  ).toFixed(2);
}
