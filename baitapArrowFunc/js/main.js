const colorList = [
  "pallet",
  "viridian",
  "pewter",
  "cerulean",
  "vermillion",
  "lavender",
  "celadon",
  "saffron",
  "fuschia",
  "cinnabar",
];
let contentHTML = "";
colorList.forEach((colorItem) => {
  contentHTML += `
    <button class="color-button ${colorItem}"></button>
    `;
});
document.getElementById("colorContainer").innerHTML = contentHTML;
//add 'active' to button
let houseId = document.getElementById("house");
let buttons = document.querySelectorAll("button");
console.log("buttons: ", buttons);
buttons.forEach((item) => {
  item.addEventListener("click", function () {
    buttons.forEach((item) => item.classList.remove("active"));
    this.classList.add("active");
    for (let i = 0; i < buttons.length; i++) {
      currentButton = buttons[i];
      if (currentButton.classList.contains("active")) {
        houseId.classList.add(colorList[i]);
      } else {
        houseId.classList.remove(colorList[i]);
      }
    }
  });
});
